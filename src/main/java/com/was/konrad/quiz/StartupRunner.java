package com.was.konrad.quiz;

import com.was.konrad.quiz.database.entities.PlayerEntity;
import com.was.konrad.quiz.database.repositories.PlayerRepository;
import com.was.konrad.quiz.services.QuizDataService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
@Log
public class StartupRunner implements CommandLineRunner {

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private QuizDataService quizDataService;

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        log.info("Executing startup actions...");
        playerRepository.save(new PlayerEntity("Konrad"));
        playerRepository.save(new PlayerEntity("Dominik"));
        playerRepository.save(new PlayerEntity("Jacek"));

        log.info("List of player from db: ");
        List<PlayerEntity> playersFromDb = playerRepository.findAll();
        for (PlayerEntity player : playersFromDb) {
            log.info("Retrieved player: " + player);
        }
    }

}

