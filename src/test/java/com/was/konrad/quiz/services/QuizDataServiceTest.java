package com.was.konrad.quiz.services;

import com.was.konrad.quiz.dto.CategoryQuestionCountInfoDto;
import com.was.konrad.quiz.frontend.Difficulty;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static com.was.konrad.quiz.frontend.Difficulty.*;
import static org.junit.jupiter.api.Assertions.*;

class QuizDataServiceTest {

    @Test
    void calculateEachDifficultyQuestionCount_basicEasy() {
        // given
        CategoryQuestionCountInfoDto categoryQuestionCount = new CategoryQuestionCountInfoDto(5, 17, 13);
        // when
        Map<Difficulty, Integer> result = QuizDataService.calculateEachDifficultyQuestionCount(20, EASY, categoryQuestionCount);
        // then
        assertEquals(5, result.get(EASY));
        assertEquals(15, result.get(MEDIUM));
        assertNull(result.get(HARD));
    }

    @Test
    void calculateEachDifficultyQuestionCount_basicMedium() {
        // given
        CategoryQuestionCountInfoDto categoryQuestionCount = new CategoryQuestionCountInfoDto(8, 10, 13);
        // when
        Map<Difficulty, Integer> result = QuizDataService.calculateEachDifficultyQuestionCount(20, MEDIUM, categoryQuestionCount);
        // then
        assertEquals(5, result.get(EASY));
        assertEquals(10, result.get(MEDIUM));
        assertEquals(5, result.get(HARD));
    }
    @Test
    void calculateEachDifficultyQuestionCount_basicHard() {
        // given
        CategoryQuestionCountInfoDto categoryQuestionCount = new CategoryQuestionCountInfoDto(8, 10, 13);
        // when
        Map<Difficulty, Integer> result = QuizDataService.calculateEachDifficultyQuestionCount(20, HARD, categoryQuestionCount);
        // then
        assertEquals(13, result.get(HARD));
        assertEquals(7, result.get(MEDIUM));
        assertNull(result.get(EASY));
    }

}

