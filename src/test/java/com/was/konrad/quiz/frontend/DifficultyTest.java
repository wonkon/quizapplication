package com.was.konrad.quiz.frontend;

import org.junit.jupiter.api.Test;

import java.util.EnumSet;

import static com.was.konrad.quiz.frontend.Difficulty.*;
import static org.junit.jupiter.api.Assertions.*;

class DifficultyTest {

    @Test
    void calculateNextDifficulty_null() {
        // given
        EnumSet<Difficulty> given = null;
        // when
        Difficulty result = calculateNextDifficulty(given);
        // then
        assertNull(result);
    }

    @Test
    void calculateNextDifficulty_none() {
        // given
        EnumSet<Difficulty> given = EnumSet.noneOf(Difficulty.class);
        // when
        Difficulty result = calculateNextDifficulty(given);
        // then
        assertNull(result);
    }

    @Test
    void calculateNextDifficulty_easy() {
        // given
        EnumSet<Difficulty> given = EnumSet.of(EASY);
        // when
        Difficulty result = calculateNextDifficulty(given);
        // then
        assertEquals(MEDIUM, result);
    }

    @Test
    void calculateNextDifficulty_medium() {
        // given
        EnumSet<Difficulty> given = EnumSet.of(MEDIUM);
        // when
        Difficulty result = calculateNextDifficulty(given);
        // then
        assertEquals(HARD, result);
    }

    @Test
    void calculateNextDifficulty_hard() {
        // given
        EnumSet<Difficulty> given = EnumSet.of(HARD);
        // when
        Difficulty result = calculateNextDifficulty(given);
        // then
        assertEquals(MEDIUM, result);
    }

    @Test
    void calculateNextDifficulty_easy_medium() {
        // given
        EnumSet<Difficulty> given = EnumSet.of(EASY, MEDIUM);
        // when
        Difficulty result = calculateNextDifficulty(given);
        // then
        assertEquals(HARD, result);
    }

    @Test
    void calculateNextDifficulty_medium_hard() {
        // given
        EnumSet<Difficulty> given = EnumSet.of(MEDIUM, HARD);
        // when
        Difficulty result = calculateNextDifficulty(given);
        // then
        assertEquals(EASY, result);
    }

    @Test
    void calculateNextDifficulty_hard_easy() {
        // given
        EnumSet<Difficulty> given = EnumSet.of(HARD, EASY);
        // when
        Difficulty result = calculateNextDifficulty(given);
        // then
        assertEquals(MEDIUM, result);
    }

    @Test
    void calculateNextDifficulty_all() {
        // given
        EnumSet<Difficulty> given = EnumSet.of(EASY, MEDIUM, HARD);
        // when
        Difficulty result = calculateNextDifficulty(given);
        // then
        assertNull(result);
    }
}